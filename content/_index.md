---
title: "Red Hat OpenShift Container Platform (RHOCP) East west comumication"
date: 2023-06-06T12:32:17+02:00
---

# East-West communication in Red Hat OpenShift Container Platform

This workshop is about enabling communication between multiple Red Hat OpenShift Clusters on a service level.

Many solutions providing this functionality are out in the wild. This workshop will focus on the three supported solutions with Red Hat OpenShift Container Platform (RHOCP). Each of them has a different approach, different use case and different complexity. All three of them lead to the same result. Please consider carefully what fits in your environment.

## Submariner

## Red Hat Application Interconnect / Skupper

## Red Hat Service Mesh / Istio
